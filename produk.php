<?php
  $bodyClass = "product-page";
  include('header.php');
?>

<header>
  <div class="topbar">
    <img src="assets/img/contact-bg.png" class="right">
    <div class="contact-index">
      <i class="fa fa-phone fa-4x"></i>
      <span>CONTACT US! <br /> +62 700 7000</span>
    </div>
  </div>
  <div class="wrapper">
    <img src="assets/img/logo.png" class="logo">
  </div>
  <div class="contain-to-grid sticky">
  <nav class="menu-bar" data-topbar role="navigation" data-options="sticky_on: large">
    <div class="wrapper">
      <ul>
        <li class="menu"><a href="index.php">Home</a></li>
        <li class="menu">
          <a href="produk.php" class="selected" data-dropdown="product-menu" data-options="is_hover:true">Products</a>
          <ul id="product-menu" class="f-dropdown" data-dropdown-content>
            <li><a href="produk.php">Baja Ringan</a></li>
            <li><a href="produk.php">Genteng</a></li>
            <li><a href="produk.php">Aksesoris</a></li>
          </ul>
        </li>
        <li class="menu"><a href="projek.php">Projects</a></li>
        <li class="menu"><a href="about.php">About Us</a></li>
        <li class="menu"><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </nav>
  </div>
</header>

<div class="product-hero">
  <div class="large-12 columns slider-caption">
    <h2>Kami Mengutamakan  <span><em>Kualitas</em></span> Dalam Produk Kami</h2>
    <h2>Dan Produk Kami Telah Teruji  <span><em>Tahan Lama</em></span></h2>
  </div>
</div>

<div id="kategori-produk" class="clearfix"></div>
<div class="wrapper">
  <div class="main-content">
    <h2><span>Kategori</span> Produk</h2>
    <hr>

    <dl class="tabs vertical" data-tab>
      <dd class="active"><a href="#panel1">Baja Ringan</a></dd>
      <dd><a href="#panel2">Genteng</a></dd>
      <dd><a href="#panel3">Aksesoris</a></dd>
    </dl>
    <div class="tabs-content">
      <div class="content active" id="panel1">
        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
            <div class="description">
              <p>TIPE 1</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
            <div class="description">
              <p>TIPE 2</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
            <div class="description">
              <p>TIPE 3</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>

        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
            <div class="description">
              <p>TIPE 4</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
            <div class="description">
              <p>TIPE 5</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>
      </div>
      <div class="content" id="panel2">
        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-2.jpg')"></div>
            <div class="description">
              <p>TIPE 1</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-2.jpg')"></div>
            <div class="description">
              <p>TIPE 2</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-2.jpg')"></div>
            <div class="description">
              <p>TIPE 3</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>

        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-2.jpg')"></div>
            <div class="description">
              <p>TIPE 4</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>
      </div>
      <div class="content" id="panel3">
        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 1</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 2</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 3</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>

        <ul class="small-block-grid-1 medium-block-grid-3">
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 4</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 5</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
          <li>
          <a href="">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
            <div class="description">
              <p>TIPE 5</p>
              <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
            </div>
          </a>
          </li>
        </ul>
      </div>
    </div>
  </div>


</div>

<?php
  include('footer.php');
?>
