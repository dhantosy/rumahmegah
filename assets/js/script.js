$(document).ready(function(){

  $('.slider').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    pauseOnHover: false,
    dots: true
  });

  $('.project-list').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 640,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
  });

  // $(".about-btn").click(function(e){
  //   e.preventDefault();
  //   $('body,html').animate({scrollTop:
  //     $("#about-us").offset().top
  //   }, 700);
  // });

  // $(".projects-btn").click(function(e){
  //   e.preventDefault();
  //   $('body,html').animate({scrollTop:
  //     $("#projects").offset().top
  //   }, 700);
  // });

  // $(".product-btn").click(function(e){
  //   e.preventDefault();
  //   $('body,html').animate({scrollTop:
  //     $("#products").offset().top
  //   }, 700);
  // });


}); //END READY

$(document).foundation({
  dropdown: {
    // specify the class used for active dropdowns
    active_class: 'open'
  }
});