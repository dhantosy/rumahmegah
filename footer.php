    <footer>
      <span>All Rights Reserved &copy; <?php echo date("Y") ?>, PT Rumah Megah</span>
    </footer>

    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/script.js"></script>

  </body>
</html>