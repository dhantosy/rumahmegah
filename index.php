<?php
  $bodyClass = "home";
  include('header.php');
?>

<header>
  <div class="topbar">
    <img src="assets/img/contact-bg.png" class="right">
    <div class="contact-index">
      <i class="fa fa-phone fa-4x"></i>
      <span>CONTACT US! <br /> +62 700 7000</span>
    </div>
  </div>
  <div class="wrapper">
    <img src="assets/img/logo.png" class="logo">
  </div>
  <div class="contain-to-grid sticky">
  <nav class="menu-bar" data-topbar role="navigation" data-options="sticky_on: large">
    <div class="wrapper">
      <ul>
        <li class="menu"><a href="index.php" class="selected">Home</a></li>
        <li class="menu">
          <a href="produk.php" data-dropdown="product-menu" data-options="is_hover:true">Products</a>
          <ul id="product-menu" class="f-dropdown" data-dropdown-content>
            <li><a href="produk.php">Baja Ringan</a></li>
            <li><a href="produk.php">Genteng</a></li>
            <li><a href="produk.php">Aksesoris</a></li>
          </ul>
        </li>
        <li class="menu"><a href="projek.php">Projects</a></li>
        <li class="menu"><a href="about.php">About Us</a></li>
        <li class="menu"><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </nav>
  </div>
</header>

<div class="slider">
  <div class="slider-1">
    <div class="row">
      <div class="large-12 columns slider-caption">
        <h2>Selamat Datang di PT Rumah Megah</h2>
        <h2>Penyedia <span><em>Baja Ringan</em></span> Terkemuka di Indonesia</h2>
      </div>
    </div>
  </div>
  <div class="slider-2">
    <div class="row">
      <div class="large-12 columns slider-caption">
        <h2>We dedicate ourselves to <span><em>perfection</em></span></h2>
        <h2>for more than <span><em>30 years</em></span> and counting</h2>
      </div>
    </div>
  </div>
</div>

<div class="wrapper">

  <div id="products" class="main-content">
    <h2><span>Our</span> Products</h2>
    <hr>
    <div class="row">
      <ul class="small-block-grid-1 medium-block-grid-3">
        <li>
        <a href="produk.php">
         <div class="thumbs" style="background-image:url('assets/img/product-1.jpg')"></div>
          <div class="subtitle">
            <h3>Baja Ringan</h3>
          </div>
          <div class="description">
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
          </div>
        </a>
        </li>
        <li>
        <a href="produk.php">
          <div class="thumbs" style="background-image:url('assets/img/product-2.jpg')"></div>
          <div class="subtitle">
            <h3>Genteng</h3>
          </div>
          <div class="description">
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
          </div>
        </a>
        </li>
        <li>
        <a href="produk.php">
           <div class="thumbs" style="background-image:url('assets/img/product-3.jpg')"></div>
          <div class="subtitle">
            <h3>Aksesoris</h3>
          </div>
          <div class="description">
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
          </div>
        </a>
        </li>
      </ul>
    </div>
  </div>

  <div id="projects" class="main-content">
    <h2><span>Our</span> Projects</h2>
    <hr>
    <div class="project-list">
      <div class="project-item">
        <img src="assets/img/project-1.jpg">
        <h4>Green Andara Jakarta</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="project-item">
        <img src="assets/img/project-2.jpg">
        <h4>HAVANA Living</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="project-item">
        <img src="assets/img/project-3.jpg">
        <h4>Grand Duta Mas</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="project-item">
        <img src="assets/img/project-2.jpg">
        <h4>Green Andara Jakarta</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
    </div>
  </div>

</div>

<div class="index-hero"></div>

<div class="wrapper">

  <div id="about-us" class="main-content">
    <h2><span>About</span> Us</h2>
    <hr>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.</p>
    <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum.</p>
    <a href="about.php" class="more">Read More</a>
  </div>

  <div id="contact-us" class="main-content">
    <h2><span>Contact</span> Us</h2>
    <hr>
    <div class="row">
      <ul class="small-block-grid-1 medium-block-grid-3 address">
        <li>
          <img src="assets/img/map-1.jpg">
          <address>
          <h4>Pondok Gede</h4>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
        </li>
        <li>
          <img src="assets/img/map-2.jpg">
          <h4>Jati Asih</h4>
          <address>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
        <li>
          <img src="assets/img/map-3.jpg">
          <h4>Jati Rangga</h4>
          <address>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
      </ul>
    </div>
  </div>

</div>

<?php
  include('footer.php');
?>
