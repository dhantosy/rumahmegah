<?php
  $bodyClass = "contact-page";
  include('header.php');
?>

<header>
  <div class="topbar">
    <img src="assets/img/contact-bg.png" class="right">
    <div class="contact-index">
      <i class="fa fa-phone fa-4x"></i>
      <span>CONTACT US! <br /> +62 700 7000</span>
    </div>
  </div>
  <div class="wrapper">
    <img src="assets/img/logo.png" class="logo">
  </div>
  <div class="contain-to-grid sticky">
  <nav class="menu-bar" data-topbar role="navigation" data-options="sticky_on: large">
    <div class="wrapper">
      <ul>
        <li class="menu"><a href="index.php">Home</a></li>
        <li class="menu">
          <a href="produk.php" data-dropdown="product-menu" data-options="is_hover:true">Products</a>
          <ul id="product-menu" class="f-dropdown" data-dropdown-content>
            <li><a href="produk.php">Baja Ringan</a></li>
            <li><a href="produk.php">Genteng</a></li>
            <li><a href="produk.php">Aksesoris</a></li>
          </ul>
        </li>
        <li class="menu"><a href="projek.php">Projects</a></li>
        <li class="menu"><a href="about.php">About Us</a></li>
        <li class="menu"><a href="contact.php" class="selected">Contact Us</a></li>
      </ul>
    </div>
  </nav>
  </div>
</header>

<div class="contact-hero">
  <div class="large-12 columns slider-caption">
    <h2>Kami Mengutamakan  <span><em>Kualitas</em></span> Dalam Produk Kami</h2>
    <h2>Dan Produk Kami Telah Teruji  <span><em>Tahan Lama</em></span></h2>
  </div>
</div>

<div class="wrapper text-left">
  <div class="main-content">
    <h2><span>Our</span> Locations</h2>
    <hr>

    <div class="row m-bottom-50">
      <ul class="small-block-grid-1 medium-block-grid-3 address">
        <li>
          <img src="assets/img/map-1.jpg">
          <address>
          <h4>Pondok Gede</h4>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
        </li>
        <li>
          <img src="assets/img/map-2.jpg">
          <h4>Jati Asih</h4>
          <address>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
        </li>
        <li>
          <img src="assets/img/map-3.jpg">
          <h4>Jati Rangga</h4>
          <address>
          Jl. Jend. A. Yani, Kav. 67<br>
          Jakarta 10510.<br>
          P: +62 21 424 7451 - 52<br>
          Email: info@rumahmegah.co.id
          </address>
        </li>
      </ul>
    </div>

    <h2><span>Kirim</span> Pesan</h2>
    <hr>

    <div class="row">
      <input type="text" placeholder="FULL NAME" id="name"/>
      <input type="text" placeholder="EMAIL" id="email"/>
      <input type="text" placeholder="TELEPON" id="telepon"/>
      <input type="text" placeholder="SUBJEK" id="subjek"/>
      <textarea placeholder="TULIS PESAN" id="pesan"></textarea>
      <input type="submit" value="KIRIM!" class="button"/>
    </div>

  </div>
</div>

<?php
  include('footer.php');
?>
