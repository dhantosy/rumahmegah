<?php
  $bodyClass = "home";
  include('header.php');
?>

<div class="small-6 columns left-side">
  <div class="flower">
    <img src="assets/img/logo.png">
  </div>
</div>

  <div class="row right-side">
    <div class="small-6 columns info">
      <img src="assets/img/logo-2.png">
      <h1>THE <span>GREEN</span> BOARDING HOUSE</h1>
      <img src="assets/img/comingsoon.png">
      <h2>www.indkostjogja.com</h2>
      <address>Jl. Seturan E II/3b Jogyakarta<br>
      Tlp. 0859 2746 1267</address>
    </div>
  </div>

<?php
  include('footer.php');
?>
