<?php
  $bodyClass = "project-page";
  include('header.php');
?>

<header>
  <div class="topbar">
    <img src="assets/img/contact-bg.png" class="right">
    <div class="contact-index">
      <i class="fa fa-phone fa-4x"></i>
      <span>CONTACT US! <br /> +62 700 7000</span>
    </div>
  </div>
  <div class="wrapper">
    <img src="assets/img/logo.png" class="logo">
  </div>
  <div class="contain-to-grid sticky">
  <nav class="menu-bar" data-topbar role="navigation" data-options="sticky_on: large">
    <div class="wrapper">
      <ul>
        <li class="menu"><a href="index.php">Home</a></li>
        <li class="menu">
          <a href="produk.php" data-dropdown="product-menu" data-options="is_hover:true">Products</a>
          <ul id="product-menu" class="f-dropdown" data-dropdown-content>
            <li><a href="produk.php">Baja Ringan</a></li>
            <li><a href="produk.php">Genteng</a></li>
            <li><a href="produk.php">Aksesoris</a></li>
          </ul>
        </li>
        <li class="menu"><a href="projek.php" class="selected">Projects</a></li>
        <li class="menu"><a href="about.php">About Us</a></li>
        <li class="menu"><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </nav>
  </div>
</header>

<div class="project-hero">
  <div class="large-12 columns slider-caption">
    <h2>Kami Mengutamakan  <span><em>Kualitas</em></span> Dalam Produk Kami</h2>
    <h2>Dan Produk Kami Telah Teruji  <span><em>Tahan Lama</em></span></h2>
  </div>
</div>

<div class="wrapper text-left">
  <div class="main-content">
    <h2><span>Our</span> Projects</h2>
    <hr>

    <div class="row">
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-1.jpg">
        <h4>Green Andara Jakarta</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-2.jpg">
        <h4>HAVANA Living</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-3.jpg">
        <h4>Grand Duta Mas</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
    </div>

    <div class="row">
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-1.jpg">
        <h4>Green Andara Jakarta</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-2.jpg">
        <h4>HAVANA Living</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-3.jpg">
        <h4>Grand Duta Mas</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
    </div>

    <div class="row">
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-1.jpg">
        <h4>Green Andara Jakarta</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-2.jpg">
        <h4>HAVANA Living</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
      <div class="small-12 medium-4 large-4 columns project-item">
        <img src="assets/img/project-3.jpg">
        <h4>Grand Duta Mas</h4>
        <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis.</p>
      </div>
    </div>
  </div>
</div>

<?php
  include('footer.php');
?>
