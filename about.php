<?php
  $bodyClass = "about-page";
  include('header.php');
?>

<header>
  <div class="topbar">
    <img src="assets/img/contact-bg.png" class="right">
    <div class="contact-index">
      <i class="fa fa-phone fa-4x"></i>
      <span>CONTACT US! <br /> +62 700 7000</span>
    </div>
  </div>
  <div class="wrapper">
    <img src="assets/img/logo.png" class="logo">
  </div>
  <div class="contain-to-grid sticky">
  <nav class="menu-bar" data-topbar role="navigation" data-options="sticky_on: large">
    <div class="wrapper">
      <ul>
        <li class="menu"><a href="index.php">Home</a></li>
        <li class="menu">
          <a href="produk.php" data-dropdown="product-menu" data-options="is_hover:true">Products</a>
          <ul id="product-menu" class="f-dropdown" data-dropdown-content>
            <li><a href="produk.php">Baja Ringan</a></li>
            <li><a href="produk.php">Genteng</a></li>
            <li><a href="produk.php">Aksesoris</a></li>
          </ul>
        </li>
        <li class="menu"><a href="projek.php">Projects</a></li>
        <li class="menu"><a href="about.php" class="selected">About Us</a></li>
        <li class="menu"><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </nav>
  </div>
</header>

<div class="about-hero">
  <div class="large-12 columns slider-caption">
    <h2>Kami Mengutamakan  <span><em>Kualitas</em></span> Dalam Produk Kami</h2>
    <h2>Dan Produk Kami Telah Teruji  <span><em>Tahan Lama</em></span></h2>
  </div>
</div>

<div class="wrapper text-left">
  <div class="main-content">
    <div class="project-list m-bottom-50">
      <div class="project-item">
        <img src="assets/img/project-1.jpg">
      </div>
      <div class="project-item">
        <img src="assets/img/project-2.jpg">
      </div>
      <div class="project-item">
        <img src="assets/img/project-3.jpg">
      </div>
      <div class="project-item">
        <img src="assets/img/project-2.jpg">
      </div>
    </div>

    <h2><span>About</span> Us</h2>
    <hr>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.</p>
    <p>Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.</p>
    <p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales.</p>
  </div>
</div>

<?php
  include('footer.php');
?>
